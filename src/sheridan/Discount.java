package sheridan;
public abstract class Discount {
    public double amount;
    public double getAmount(){
        return amount;
    }
    public abstract void calculateDiscount(double amount);
}
