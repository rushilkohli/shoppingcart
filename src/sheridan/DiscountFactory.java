package sheridan;
public class DiscountFactory {
   private static DiscountFactory discFactory;
    
    private DiscountFactory() {
        
    }
    
    public static DiscountFactory getInstance() {
        if( discFactory == null ) {
            discFactory = new DiscountFactory();
        }
        return discFactory;
    }
    
    public Discount getDiscount(DiscountTypes type, double discount) {
          Discount disc = null;
        switch (type) {
            case AMOUNT :
                disc.calculateDiscount(discount);
                disc = new DiscountByAmount(discount);
            break;
            case PERCENTAGE : 
                disc = new DiscountByPercentage( discount);
                break;
        }
        return disc;
    } 
}
