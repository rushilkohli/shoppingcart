package sheridan;

import java.util.ArrayList;

public class Cart {

    private final ArrayList<Product> products;
    private PaymentService service;
    private Discount discount;
    private int numberOfItems;

    public Cart() {
        products = new ArrayList<>();
    }

    public void setPaymentService(PaymentService service) {
        this.service = service;
    }

    public void addProduct(Product product) {
        products.add(product);
        numberOfItems++;
    }

    public void payCart() {
        double totalPrice = 0;
        double totalDiscount = 20 ;
        totalPrice = products.stream().map(p -> p.getPrice()).reduce(totalPrice, (accumulator, _item) -> accumulator + _item);
        service.processPayment(totalPrice);
        if(totalDiscount > 0) {
            totalDiscount = products.stream().map(p -> p.getDiscount()).reduce(totalDiscount, (accumulator, _item) -> accumulator + _item);
            discount.calculateDiscount(totalPrice - totalDiscount);
        }
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }
    public boolean cartIsEmpty()   {
        if(numberOfItems == 0) {
            return true;
        }
        else{
        return  false;
        }
    }
    
}
